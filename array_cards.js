let data = require('./data');

//1. Find all card numbers whose sum of all the even position digits is odd.



//2. Find all cards that were issued before June.
function findCardBaseOnMonth(data, month) {
    const monthsName = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ];
    let monthIndex = monthsName.indexOf(month) + 1;

    let result = data.filter((card) => {
        let cardMonth = card.issue_date.split('/')
        return cardMonth[0] < monthIndex
    })
    return result

}



//3. Assign a new field to each card for CVV where the CVV is a random 3 digit number.
function generateCvv(data) {
    let result = data.map((card) => {
        let cvv = Math.floor(Math.random() * 1000)
        if (cvv < 100) {
            cvv = "0" + cvv
        }
        card["CVV"] = cvv;
        return card;
    })

    return result;
}



//4. Add a new field to each card to indicate if the card is valid or not.
function addValid(data) {
    data = generateCvv(data)

    let result = data.map((card) => {
        card["validity"] = "";
        return card;
    })
    return result;

}


//5. Invalidate all cards issued before March.
function isInvalid(data) {
    cardData = addValid(data)
    let result = cardData.map((card) => {
        if (card.issue_date.split('/')[0] > 3) {
            card.validity = true;
        } else {
            card.validity = false;
        }
        return card;
    })

    return result;
}


// 7. Group the data in such a way that we can identify which cards were assigned in which months.
function groupByMonths(data){
    const monthsName = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
];

    let months = {};
    let result = data.map((card)=>{
        cardMonth = card.issue_date.split('/')[0]-1;
        if(months[monthsName[cardMonth]]){
            months[monthsName[cardMonth]] = [...months[monthsName[cardMonth]],card]
        }
        else{
            months[monthsName[cardMonth]] = [card]
        }
    })
    return months;
}
console.log(groupByMonths(data));
